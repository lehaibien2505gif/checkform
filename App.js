import React, { useState, useEffect, Component } from 'react';
import {
  Button,
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TextInput,
  Alert
} from 'react-native';
import { App_con } from './src/App_con';

const App = () => {

  // const [value, setvalue] = useState(0);
  // const onChangeValue = () => {
  //   setvalue(value + 1);
  // }

  // Email
  const [email, setEmail] = useState('');
  const [isValiEmail, setValiEmail] = useState(true);
  // Phone 
  const [Phone, setPhone] = useState('');
  const [isVailPhone, setValiPhone] = useState(true);
  const [NoteEmail, setNoteEmail] = useState('');
  const [NotePhone, setNotePhone] = useState('');


  // check Email
  const verifyEmail = (email) => {
    let regex = new RegExp(/([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\"\(\[\]!#-[^-~ \t]|(\\[\t -~]))+@([!#-'*+/-9=?A-Z^-~-]+(\.[!#-'*+/-9=?A-Z^-~-]+)*|\[[\t -Z^-~]*])/);
    if (!email) return true;
    if (regex.test(email)) {
      return true;
    }
    return false;
  }

  // check Phone
  const verifyPhone = (Phone) => {
    let regex = new RegExp(/([\+84|84|0]+(3|5|7|8|9|1[2|6|8|9]))+([0-9]{8})\b/);
    if (!Phone) return true;
    if (regex.test(Phone)) {
      return true;
    }
    return false;
  }

  // Alert
  const onPressAlert = () =>
    Alert.alert('Notification', 'Xác nhận đăng nhập ?', [
      { text: "Cancel", onPress: () => console.log('Kiểm tra lại thông tin...'), style: "cancel" },
      { text: "OK", onPress: () => { setNoteEmail(email), setNotePhone(Phone) } }
    ]);





  return (
    // <SafeAreaView style={styles.container}>

    //   <Text style={styles.text}> {'So lan click: ' + value} </Text>
    //   <View style={styles.btn}>
    //     <Button title='An vao day di ^^' onPress={onChangeValue} />
    //   </View>

    //   {/* video 02 */}

    //   <App_con name='Le Hai Bien' color='black' fontSize={40} />

    //   <App_con name='Le Huu Dang' color='#FF1E1E' />

    // </SafeAreaView>

    //  Video 03

    <View style={styles.container}>

      <Text style={styles.text3}> LOGIN APP</Text>

      <TextInput style={styles.txt}
        placeholder='Email'
        placeholderTextColor={'#674747'}
        onChangeText={(text) => {
          setEmail(text);
          const isVaLid = verifyEmail(text);
          isVaLid ? setValiEmail(true) : setValiEmail(false);
        }}
        value={email}

        // // dinh dang kieu du lieu nhap vao
        keyboardType='email-address'
        // // tu dong xuong dong khi nhieu ki tu
        multiline={true}
        // // thay doi mau sac con tro
        selectionColor={'#D2001A'}
      // // gach chan text input
      // underlineColorAndroid={'#905E96'}


      />

      <Text style={styles.textEr}>{isValiEmail ? '' : 'Vui lòng nhập đúng định dạng Email !'}</Text>

      <TextInput style={styles.txt}

        placeholder='Phone'
        placeholderTextColor={'#674747'}
        onChangeText={(Phone) => {
          setPhone(Phone)
          const isVaLids = verifyPhone(Phone)
          isVaLids ? setValiPhone(true) : setValiPhone(false);
        }}
        value={Phone}
        keyboardType='phone-pad'

        // // an mk nguoi dung nhap vao
        // secureTextEntry={true}
        // // chi dinh loai ki tu nhap vao
        // keyboardType='phone-pad'
        // // gioi han so luong ki tu nhap vao
        maxLength={12}
        // // thay doi mau sac con tro
        selectionColor={'#D2001A'}

      />

      <Text style={styles.textEr}>{isVailPhone ? '' : 'Vui lòng nhập đúng định dạng số điện thoại !'} </Text>

      <View style={styles.Viewbtn}>
        <Button style={styles.btn}
          onPress={() => onPressAlert()}
          title="SAVE"
          color={'green'}
        />
      </View>

      <View style={{ flex: 1 }}>
        <Text style={{ fontSize: 20, fontWeight: '600', color: '#000000' }}>{'Email: ' + NoteEmail}</Text>
        <Text style={{ fontSize: 20, fontWeight: '600', color: '#000000', marginTop: 20 }}>{'Phone: ' + NotePhone}</Text>
      </View>

    </View>


  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    // justifyContent: 'center',
    alignItems: 'center',


  },

  text: {
    fontSize: 20,
    fontWeight: '500',
    color: 'red'
  },

  text3: {
    fontSize: 30,
    fontWeight: '500',
    color: '#D2001A',
    marginTop: 30
  },

  txt: {
    alignItems: 'center',
    fontSize: 20,
    width: '95%',
    height: 70,
    backgroundColor: '#EEF1FF',
    marginVertical: 20,
    padding: 15,
    borderWidth: 2,
    borderColor: '#829460',
    color: 'black'
  },


  Viewbtn: {
    flex: 1,
    width: '80%',
    height: 200,
    marginTop: 20
  },

  textEr: {
    fontSize: 17,
    color: 'red',
    padding: 10,
    fontWeight: '500'

  }

});
export default App;
