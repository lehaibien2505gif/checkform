import React from "react";
import { Text, View, Button } from 'react-native';

const App_con = (props) => {
    return (
        <Text style={{ color: props.color, fontSize: props.fontSize, fontWeight: '700' }} >
            {props.name}
        </Text>
    );
}
export { App_con }